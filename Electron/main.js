const electron = require('electron');
const fs = require('fs');
const path = require('path');
const os = require('os')
const fixPath = require('fix-path')
const PLATFORM = os.platform()
const { app, Tray, Menu, nativeTheme, dialog } = electron;
let _tray;

if (PLATFORM === 'darwin') {
    fixPath();
}

process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = true;

function createTray() {
    let logo = nativeTheme.shouldUseDarkColors ? 'logo.png' : 'logo-light.png';
    _tray = new Tray(path.join(__dirname, logo));

    const trayMenuTemplate = [
        {
            label: 'Launch at Startup',
            click: function () {
                const AutoLaunch = require('auto-launch');
                let autoLauncher = new AutoLaunch({
                    name: "HydraTool"
                });
                autoLauncher.enable();
            }
        },
        {
            label: 'Quit',
            click: async function () {
                await killall();
                app.exit();
            }
        },
        {
            label: 'Restart',
            click: async function () {
                await killall();
                app.relaunch();
                app.exit();
            }
        }
    ]

    trayMenuTemplate.push({
        label: 'Choose Hydra folder path',
        click: function () {
            try {
                fs.writeFileSync(path.join(app.getAppPath(), 'hydrapath.txt'), dialog.showOpenDialogSync({ properties: ['openDirectory'] })[0]);
                runScripts();
            } catch (e) {
                console.log(e);
            }
        }
    })

    let trayMenu = Menu.buildFromTemplate(trayMenuTemplate);
    _tray.setContextMenu(trayMenu);
}

async function runScript(cmd) {
    try {
        const win = PLATFORM === 'win32';

        if (win) {
            const exec = require('child_process').execSync
            exec(cmd, { stdio: 'inherit' })
        } else {
            const exec = require('execa')
            exec('node', [cmd])
        }
    } catch (err) {
        console.log(err)
    }
}

async function runScripts() {
    const win = PLATFORM == 'win32';
    let macPath;

    if (!win) {
        try {
            macPath = fs.readFileSync(path.join(app.getAppPath(), 'hydrapath.txt')).toString();
            macPath = path.join(macPath, 'Server', 'server.js');
        } catch (e) {
            return;
        }
    }

    let cmd = win ? 'wsl sudo node /mnt/c/ActiveTheory/Hydra/Server/server.js' : macPath;
    await runScript(cmd);

    if (win) {
        if (!fs.existsSync(path.join('c:', 'ActiveTheory', 'Hydra', 'Server', 'Plugins', 'osc', 'package-lock.json'))) {
            await runScript('wsl sudo node /mnt/c/ActiveTheory/Hydra/Server/Plugins/osc/install.js');
        }

        runScript('wsl sudo node /mnt/c/ActiveTheory/Hydra/Server/Plugins/osc/index.js');
    }
}

async function killall() {
    try {
        const win = PLATFORM == 'win32';
        let exec = win ? require('child_process').execSync : require('execa');

        if (win) {
            return exec('wsl sudo killall -9 -r node');
        } else {
            return exec('killall', ['node']);
        }
    } catch (err) {
        console.log(err)
    }
}

app.on('ready', _ => {
    if (app.dock) app.dock.hide();
    createTray();
    runScripts();

    nativeTheme.on('updated', () => {
        _tray.destroy();
        createTray();
    });
});

app.on('window-all-closed', e => {
    e.preventDefault();
});

app.on('quit', _ => {
    killall();
});
