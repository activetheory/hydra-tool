#!/usr/bin/env bash

cd ../Tools
echo "[Building project...]"
node build install
cd ..

cp HTML/assets/js/hydra/hydra-thread.js Electron/www/hydra-thread.js

cd Electron

rm -rf Installer

echo "[Install deps...]"

npm install

echo "[Pack electron...]"

if [ "$@" ] ; then
    echo "- building: $@"
    npm run "$@"
else
    echo "- building windows"
    npm run windows
    electron-installer-windows --src HydraTool-win32-x64/ --dest Installer
fi
