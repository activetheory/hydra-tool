const _DIR_ = __dirname.split('/Tools')[0];
const _HTML_ = _DIR_ + '/HTML/';
const _TOOLS_ = _DIR_ + '/Tools/';
const _JS_ = _DIR_ + '/HTML/assets/js/';
const _BUILD_ = _DIR_ + '/Build/';

const _fs = require('fs');
const _execSync = require('child_process').execSync;
const _fsExtra = require('fs-extra');
const _babel = require("@babel/core");
const UglifyJS = require("uglify-es");
const walkdir = require("walkdir");

let _allJS = [];
let _hydraJS = [];
let _projectJS = [];
let _moduleJS = [];

CONFIG = (function() {
    var args = process.argv;
    for (var i in args) {
        if (!!~args[i].indexOf('--config=')) {
            let config = args[i].split('--config=')[1];
            return require('./' + config + '.json');
        }
    }
    return require('./build.json');
})();

(function() {
    clearBuildDirectory();
    parseJS();
    if (!CONFIG.PREVENT_TREE_SHAKE) treeShake(); // TODO: make this work with projectJS and moduleJS classes
    copyFiles();
    if (CONFIG.EXCLUDE && CONFIG.EXCLUDE.length) excludeJS();
    compileJS();
    if (CONFIG.CUSTOM && CONFIG.CUSTOM.length) runCustom();
    if (CONFIG.COPY && CONFIG.COPY.length) copyBuild();
    cleanup();
})();

function clearBuildDirectory() {
    _fsExtra.removeSync(_BUILD_);
    _fs.mkdirSync(_BUILD_);
    _fs.chmodSync(_BUILD_, '777');
}

function parseJS() {
    let index = _fs.readFileSync(_HTML_ + 'index.html', 'utf8');
    _allJS = JSON.parse(index.split('RUNTIME_SCRIPTS = ')[1].split(';\n')[0]);

    _allJS.forEach(path => {
        if (!!~path.indexOf('js/hydra/')) return _hydraJS.push(path);
        if (!!~path.indexOf('app/modules/')) return _moduleJS.push(path);
        _projectJS.push(path);
    });
}

function treeShake() {
    let projectCode = '';

    let requiredModules = [];
    walkdir.sync(_JS_+'app/modules', path => {
        if (path.includes('module.json')) {
            let json = JSON.parse(_fs.readFileSync(path).toString());
            if (json.modules && json.modules.length) requiredModules = requiredModules.concat(json.modules);
            if (json.build && json.build.length) requiredModules = requiredModules.concat(json.build);
        }
    });

    let matchModules = path => {
        for (let name of requiredModules) {
            if (path.includes(name)) return true;
        }
        return false;
    };

    // Compile custom js to check for unused classes and modules
    _hydraJS.concat(_projectJS).forEach(path => {
        projectCode += _fs.readFileSync(_HTML_ + path, 'utf8');
        projectCode += '\n';
    });

    _moduleJS.forEach(path => {
        let className = path.split('/').splice(-1)[0].split('.')[0];
        let len = find(className, projectCode).length;
        let requiredLen = path.includes('app/modules') ? 1 : 2;
        if (len < requiredLen && !matchModules(path)) {
            _allJS.splice(_allJS.indexOf(path), 1);
        }
    });

    function find(needle, haystack) {
        let re = new RegExp(needle,'gi');
        let results = [];
        while (re.exec(haystack)){
            results.push(re.lastIndex);
        }
        return results;
    }
}

function copyFiles() {
    _fsExtra.copySync(_HTML_, _BUILD_);

    // Delete runtime folder
    _fsExtra.removeSync(_BUILD_ + 'assets/runtime');

    // Prepare index.html
    _fs.renameSync(_BUILD_ + 'build.html', _BUILD_ + 'index.html');
    let index = _fs.readFileSync(`${_BUILD_}index.html`, 'utf8');
    index = index.replace('%CACHE%', Date.now());
    _fs.writeFileSync(`${_BUILD_}index.html`, index);

    // Copy only compiled shaders
    if (_fs.existsSync(_BUILD_ + 'assets/shaders')) {
        _fsExtra.removeSync(_BUILD_ + 'assets/shaders');
        _fsExtra.copySync(_HTML_ + 'assets/shaders/compiled.vs', _BUILD_ + 'assets/shaders/compiled.vs');
    }

    _fsExtra.removeSync(_BUILD_ + 'assets/js/');

    // Copy compiled libs
    if (_fs.existsSync(_JS_ + 'lib/')) {
        let libs = _fs.readdirSync(_JS_ + 'lib/');
        libs.forEach(lib => {
            if (!~lib.indexOf('.js') || lib.indexOf('_') === 0) return;
            let path = _BUILD_ + 'assets/js/lib/' + lib;
            _fsExtra.copySync(_JS_ + 'lib/' + lib, path);

            // Minify lib
            let code = _fs.readFileSync(path, 'utf8');
            let result = UglifyJS.minify({file: code}, {mangle: false});
            code = result.code;

            _fs.writeFileSync(path, code);
            _fs.chmodSync(path, '777');
        });
    }

    //remove spacing from uil json
    if (_fs.existsSync(`${_BUILD_}assets/data/uil.json`)) {
        let json = JSON.stringify(JSON.parse(_fs.readFileSync(`${_BUILD_}assets/data/uil.json`)));
        _fs.writeFileSync(`${_BUILD_}assets/data/uil.json`, json);
    }

    // Copy thread files
    ['assets/js/hydra/hydra-thread.js', 'assets/js/hydra/hydra-thread-es5.js'].forEach(path => {
        if (_fs.existsSync(_HTML_ + path)) _fsExtra.copySync(_HTML_ + path, _BUILD_ + path);
    });
}

function excludeJS() {
    CONFIG.EXCLUDE.forEach(str => {
        for (let i = _allJS.length - 1; i >= 0; i--) {
            if (!!~_allJS[i].indexOf(str)) _allJS.splice(i, 1);
        }
    });
}

function compileJS() {
    let splitMatch = path => {
        let array = CONFIG.SPLIT_MODULES || [];
        for (let i = 0; i < array.length; i++) {
            if (path.includes(array[i])) return true;
        }
        return false;
    };

    let compiled = '';
    let split = '';
    let obj = {};
    _allJS.forEach(path => {
        if (splitMatch(path)) {
            split += _fs.readFileSync(_HTML_ + path, 'utf8');
            split += '\n';
        } else {
            compiled += _fs.readFileSync(_HTML_ + path, 'utf8');
            compiled += '\n';
        }
    });

    let result = UglifyJS.minify({file: compiled}, {
        mangle: false,
        keep_fnames: true
    });
    let minCode = result.code;
    let unminCode = new String(compiled);

    let date = '';
    date += '// --------------------------------------\n';
    date += '// \n';
    date += '//    _  _ _/ .  _  _/ /_ _  _  _        \n';
    date += '//   /_|/_ / /|//_  / / //_ /_// /_/     \n';
    date += '//   https://activetheory.net    _/      \n';
    date += '// \n';
    date += '// --------------------------------------\n';
    date += '//   ' + getDate() + '\n';
    date += '// --------------------------------------\n';
    date += '\n';

    minCode = date + minCode;
    unminCode = date + unminCode;

    minCode += '\nwindow._MINIFIED_ = true;';
    minCode += '\nwindow._BUILT_ = true;';
    unminCode += '\nwindow._BUILT_ = true;';

    _fs.writeFileSync(`${_BUILD_}assets/js/app--debug.js`, unminCode);
    _fs.writeFileSync(`${_BUILD_}assets/js/app.js`, minCode);


    let moduleCode = UglifyJS.minify({file: split}, {
        mangle: false,
        keep_fnames: true
    }).code + '\nwindow._MODULES_ = true;';
    _fs.writeFileSync(`${_BUILD_}assets/js/modules.js`, moduleCode);

    if (CONFIG.ES5) compileES5(compiled, split)
}

function compileES5(input, split) {

    let output = _babel.transformSync(input, {
        sourceMaps: 'inline',
        compact: true,
        presets: ['@babel/env']
    });

    output.code = output.code.replace(/'use strict';/g, '');
    output.code = output.code.replace(/"use strict";/g, '');

    let result = UglifyJS.minify({file: output.code}, {
        mangle: false,
        keep_fnames: true
    });

    let minCode = result.code;
    let unminCode = new String(output.code);

    minCode += '\nwindow._MINIFIED_ = true;';
    minCode += '\nwindow._BUILT_ = true;';
    unminCode += '\nwindow._BUILT_ = true;';

    if (_DIR_.toLowerCase().indexOf('sections') < 0) {
        // Prepend es5 polyfill`
        let es5Prefix = _fs.readFileSync(_HTML_ + 'assets/runtime/es5-polyfill.js', 'utf8') + '\n\n';
        minCode = es5Prefix + minCode;
        unminCode = es5Prefix + unminCode;
    }

    _fs.writeFileSync(`${_BUILD_}assets/js/es5-app--debug.js`, unminCode);
    _fs.writeFileSync(`${_BUILD_}assets/js/es5-app.js`, minCode);

    if (CONFIG.SPLIT_MODULES && CONFIG.SPLIT_MODULES.length) {
        let output = _babel.transformSync(split, {
            sourceMaps: 'inline',
            compact: true,
            presets: ['@babel/env']
        });

        output.code = output.code.replace(/'use strict';/g, '');
        output.code = output.code.replace(/"use strict";/g, '');

        let moduleCode = UglifyJS.minify({file: output.code}, {
            mangle: false,
            keep_fnames: true
        }).code + '\nwindow._MODULES_ = true;';

        _fs.writeFileSync(`${_BUILD_}assets/js/es5-modules.js`, moduleCode);
    }
}

function runCustom() {
    let argString = (function() {
        let arr = [];
        for (let i = 2; i < process.argv.length; i++) arr.push(process.argv[i]);
        return arr.join(' ');
    })();

    CONFIG.CUSTOM.forEach(script => {
        _execSync(`node ${_TOOLS_}custom/${script} ${argString}`, {stdio: 'inherit'});
    });
}

function copyBuild() {
    CONFIG.COPY.forEach(path => {
        let toPath = (function () {
            if (path.charAt(0) == '~') return path.slice(1);
            if (path.charAt(0) == '/') return _DIR_ + path;
            return _DIR_ + '/' + path;
        })();

        _fsExtra.removeSync(toPath);
        _fsExtra.copySync(_BUILD_, toPath);
    });
}

function cleanup() {
    try {
        _fsExtra.removeSync(`${_BUILD_}assets/data/uil-partial.js`);
    } catch(e) {
        
    }
}

function getDate() {
    var date = new Date();
    var am = 'a';
    var hours = date.getUTCHours()-7;
    if (hours <= 0) hours += 24;

    if (hours > 12) {
        hours -= 12;
        am = 'p';
    }
    if (hours == 12) am = 'p';
    if (hours == 0) hours = 12;

    var minutes = date.getMinutes().toString();
    if (minutes.length < 2) minutes = '0' + minutes;

    return (date.getMonth()+1) + '/' + date.getDate() + '/' + date.getYear().toString().slice(1)+' '+hours+':'+minutes+am;
}
