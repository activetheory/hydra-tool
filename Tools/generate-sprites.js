const _DIR_ = __dirname.toLowerCase().split('/tools')[0];
const _HTML_ = _DIR_ + '/HTML/';
const _IMAGES_ = _HTML_ + 'assets/images/';
const _SPRITES_ = _HTML_ + 'assets/sprites/';

const svgstore = require('svgstore');
const sprites = svgstore();

const _fs = require('fs');

const icons = _fs.readdirSync(_SPRITES_).map(file => {
    if (file.includes('svg')) {
        const path = `${_SPRITES_}${file}`;
        const id = file.replace('.svg', '');

        return {
            id: `sprite-${id}`,
            content: _fs.readFileSync(path, 'utf8')
        };
    }
});

icons.forEach(({ id, content }) => {
    sprites.add(id, content);
});

_fs.writeFileSync(`${_IMAGES_}sprite.svg`, sprites);
