const _DIR_ = __dirname.split('/Tools')[0];
const _HTML_ = _DIR_ + '/HTML/';
const _TOOLS_ = _DIR_ + '/Tools/';
const _JS_ = _DIR_ + '/HTML/assets/js/';
const _BUILD_ = _DIR_ + '/Build/';

const _fs = require('fs');
const _path = require('path');
const _terser = require('terser');
const _fsExtra = require('fs-extra');
const _babel = require('@babel/core');
const _execSync = require('child_process').execSync;
const _walkdir = require('walkdir');
const _args = require('./parseargs');

let ENV = 'local';
let CONFIG_ORDER = ['build.json', 'build-config.js'];
let CONFIG;

// START [OPTS]
const _terserOpts = {
    mangle: false,
    keep_fnames: true,
    safari10: true
};

const _babelOpts = {
    sourceMaps: 'inline',
    compact: true,
    presets: ['@babel/env']
};
// END [OPTS]

// START [BUILD]
let _allJS = [];
let _hydraJS = [];
let _projectJS = [];
let _moduleJS = [];

if (require.main === module) {
    process.env.NODE_ENV = 'production';

    !(function(){
        let args = _args(process.argv.slice(2));

        ENV = args['_'][0] || args['env'] || 'local';
        CONFIG = findConfig({ config: args['config'], env: ENV });

        build(CONFIG);
        process.exit();
    }());
}

function build(config) {
    console.time('build time');
    clearBuildDirectory();
    parseJS();
    if (!config.PREVENT_THREE_SHAKE) treeShake();
    copyFiles();
    if (config.EXCLUDE && config.EXCLUDE.length) excludeJS(config);
    compileJS(config);
    inlineCSS();
    if (config.CUSTOM && config.CUSTOM.length) runCustom(config);
    if (config.COPY && config.COPY.length) copyBuild(config);
    cleanup();
    console.timeEnd('build time');
}

function clearBuildDirectory() {
    removeSync(_BUILD_);
    _fs.mkdirSync(_BUILD_);
    _fs.chmodSync(_BUILD_, '777');
}

function parseJS() {
    let index = _fs.readFileSync(_HTML_ + 'index.html', 'utf8');
    _allJS = JSON.parse(index.split('RUNTIME_SCRIPTS = ')[1].split(';\n')[0]);
    for (let i = 0, len = _allJS.length; i < len; ++i) {
        let path = _allJS[i];
        if (~path.indexOf('js/hydra/')) _hydraJS.push(path);
        else if (~path.indexOf('app/modules/')) _moduleJS.push(path);
        else _projectJS.push(path);
    }
}

function treeShake() {
    let projectCode = '';
    let requiredModules = [];
    _walkdir.sync(_JS_+'app/modules', path => {
        if (path.includes('module.json')) {
            let json = JSON.parse(_fs.readFileSync(path).toString());
            if (json.modules && json.modules.length) requiredModules = requiredModules.concat(json.modules);
            if (json.build && json.build.length) requiredModules = requiredModules.concat(json.build);
        }
    });

    // Compile custom js to check for unused classes and modules
    _hydraJS.concat(_projectJS).forEach(path => {
        projectCode += _fs.readFileSync(_HTML_ + path, 'utf8');
        projectCode += '\n';
    });

    _moduleJS.forEach(path => {
        let className = path.split('/').splice(-1)[0].split('.')[0];
        let len = find(className, projectCode).length;
        let requiredLen = path.includes('app/modules') ? 1 : 2;
        if (len < requiredLen && !some(requiredModules, path)) {
            _allJS.splice(_allJS.indexOf(path), 1);
        }
    });
}

function copyFiles() {
    _fsExtra.copySync(_HTML_, _BUILD_);

    // Delete runtime folder
    removeSync(_BUILD_ + 'assets/runtime');

    // Prepare index.html
    _fs.renameSync(_BUILD_ + 'build.html', _BUILD_ + 'index.html');
    let index = _fs.readFileSync(`${_BUILD_}index.html`, 'utf8');
    index = index.replace('%CACHE%', Date.now());
    _fs.writeFileSync(`${_BUILD_}index.html`, index);

    // Copy only compiled shaders
    if (_fs.existsSync(_BUILD_ + 'assets/shaders')) {
        removeSync(_BUILD_ + 'assets/shaders');
        _fsExtra.copySync(_HTML_ + 'assets/shaders/compiled.vs', _BUILD_ + 'assets/shaders/compiled.vs');
    }

    removeSync(_BUILD_ + 'assets/js/');

    // Copy compiled libs
    if (_fs.existsSync(_JS_ + 'lib/')) {
        let libs = _fs.readdirSync(_JS_ + 'lib/');

        libs.forEach(lib => {
            if (!~lib.indexOf('.js') || lib.indexOf('_') === 0) return;
            let path = _BUILD_ + 'assets/js/lib/' + lib;
            _fsExtra.copySync(_JS_ + 'lib/' + lib, path);

            // Minify lib
            let code = _fs.readFileSync(path, 'utf8');
            let result = _terser.minify({ file: code }, {mangle: false});
            code = result.code;

            _fs.writeFileSync(path, code);
            _fs.chmodSync(path, '777');
        });
    }

    //remove spacing from uil json
    if (_fs.existsSync(`${_BUILD_}assets/data/uil.json`)) {
        let json = JSON.stringify(JSON.parse(_fs.readFileSync(`${_BUILD_}assets/data/uil.json`)));
        _fs.writeFileSync(`${_BUILD_}assets/data/uil.json`, json);
    }

    // Copy hydra thread files
    ['assets/js/hydra/hydra-thread.js', 'assets/js/hydra/hydra-thread-es5.js'].forEach(path => {
        if (_fs.existsSync(_HTML_ + path)) _fsExtra.copySync(_HTML_ + path, _BUILD_ + path);
    });
}

function excludeJS(config) {
    config.EXCLUDE.forEach(str => {
        if (str.includes('|')) {
            try {
                let envs = str.split('|')[1].split('env=')[1].split(',');
                if (!envs.includes(ENV)) return;
            } catch(e) {
                throw 'Env variable failed. Use PATH|env=xxx';
            }
        }

        for (let i = _allJS.length - 1; i >= 0; i--) {
            if (~_allJS[i].indexOf(str)) _allJS.splice(i, 1);
        }
    });
}

function compileJS(config) {
    let compiled = '';
    let split = '';

    const len = _allJS.length;
    for (let i = 0; i < len; ++i) {
        let path = _allJS[i];
        if (some(config.SPLIT_MODULES, path)) {
            split += _fs.readFileSync(_HTML_ + path, 'utf8');
            split += '\n';
        } else {
            compiled += _fs.readFileSync(_HTML_ + path, 'utf8');
            compiled += '\n';
        }
    }

    let { minCode, unminCode } = minifyJS(compiled);

    let _banner = createBanner();
    minCode = _banner + minCode;
    unminCode = _banner + unminCode;

    _fs.writeFileSync(`${_BUILD_}assets/js/app--debug.js`, unminCode);
    _fs.writeFileSync(`${_BUILD_}assets/js/app.js`, minCode);

    if (split.length) {
        let moduleCode = _terser.minify({ file: split}, _terserOpts).code + '\nwindow._MODULES_ = true;';
        _fs.writeFileSync(`${_BUILD_}assets/js/modules.js`, moduleCode);
    }

    if (config.ES5) compileES5({compiled, split, config});
}

function compileES5({ compiled, split, config }) {
    let output = _babel.transformSync(compiled, _babelOpts);
    output.code = output.code.replace(/'use strict';/g, '');
    output.code = output.code.replace(/"use strict";/g, '');

    let { minCode, unminCode } = minifyJS(output.code);

    if (_DIR_.toLowerCase().indexOf('sections') < 0) {
        // Prepend es5 polyfill`
        let es5Prefix = _fs.readFileSync(_HTML_ + 'assets/runtime/es5-polyfill.js', 'utf8') + '\n\n';
        minCode = es5Prefix + minCode;
        unminCode = es5Prefix + unminCode;
    }

    _fs.writeFileSync(`${_BUILD_}assets/js/es5-app--debug.js`, unminCode);
    _fs.writeFileSync(`${_BUILD_}assets/js/es5-app.js`, minCode);

    if (config.SPLIT_MODULES && config.SPLIT_MODULES.length) {
        let output = _babel.transformSync(split, _babelOpts);
        output.code = output.code.replace(/'use strict';/g, '');
        output.code = output.code.replace(/"use strict";/g, '');
        let moduleCode = _terser.minify({ file: output.code}, _terserOpts).code + '\nwindow._MODULES_ = true;';
        _fs.writeFileSync(`${_BUILD_}assets/js/es5-modules.js`, moduleCode);
    }
}

function runCustom(config) {
    let argString = getArgs();
    config.CUSTOM.forEach(script => {
        _execSync(`node ${_TOOLS_}custom/${script} ${argString}`, {stdio: 'inherit'});
    });
}

function copyBuild(config) {
    config.COPY.forEach(path => {
        let toPath = (function () {
            if (path.charAt(0) == '~') return path.slice(1);
            if (path.charAt(0) == '/') return _DIR_ + path;
            return _DIR_ + '/' + path;
        })();

        removeSync(toPath);
        _fsExtra.copySync(_BUILD_, toPath);
    });
}

function cleanup() {
    let removePostBuild = [
        `${_BUILD_}assets/data/uil-partial.js`
    ];

    removePostBuild.forEach(() => {
        try {
            removeSync(`${_BUILD_}assets/data/uil-partial.js`);
        } catch(e) {}
    });
}

function inlineCSS() {
    let dir = _BUILD_ + 'assets/css';

    let css = _fs.readdirSync(dir);
    let minified = css.reduce((accum, file) => {
        let data = _fs.readFileSync(`${dir}/${file}`, 'utf-8');
        accum += minifyCSS(data);
        return accum;
    }, '');

    let index = _fs.readFileSync(`${_BUILD_}index.html`, 'utf8');
    index = index.replace('<link rel="stylesheet" href="assets/css/style.css">', `<style type="text/css">${minified}</style>`);
    _fs.writeFileSync(`${_BUILD_}index.html`, index);
}

function minifyCSS (str) {
    str = str.replace(/\/\*(.|\n)*?\*\//g, '');
    str = str.replace(/\s*(\{|\}|\[|\]|\(|\)|\:|\;|\,)\s*/g, '$1');
    str = str.replace(/#([\da-fA-F])\1([\da-fA-F])\2([\da-fA-F])\3/g, '#$1$2$3');
    str = str.replace(/:[\+\-]?0(rem|em|ec|ex|px|pc|pt|vh|vw|vmin|vmax|%|mm|cm|in)/g, ':0');
    str = str.replace(/\n/g, '');
    str = str.replace(/;\}/g, '}');
    str = str.replace(/^\s+|\s+$/g, '');
    str = str.replace(new RegExp('../fonts/', 'g'), '/assets/fonts/');
    return str;
}
// END [BUILD]

// START [UTILS]
function removeSync(path) {
    if( _fs.existsSync(path) ) {
        _fs.readdirSync(path).forEach(function(file){
            let curPath = path + '/' + file;
            if(_fs.lstatSync(curPath).isDirectory()) { // recurse
                removeSync(curPath);
            } else { // delete file
                _fs.unlinkSync(curPath);
            }
        });
        _fs.rmdirSync(path);
    }
}

function find(needle, haystack) {
    let re = new RegExp(needle,'gi');
    let results = [];
    while (re.exec(haystack)){
        results.push(re.lastIndex);
    }
    return results;
}

function some(src, path) {
    return src.filter((p) => path.includes(p)).length;
}

function getDate() {
    let date = new Date();
    let am = 'a';
    let hours = date.getUTCHours()-7;
    if (hours <= 0) hours += 24;

    if (hours > 12) {
        hours -= 12;
        am = 'p';
    }
    if (hours == 12) am = 'p';
    if (hours == 0) hours = 12;

    let minutes = date.getMinutes().toString();
    if (minutes.length < 2) minutes = '0' + minutes;

    return (date.getMonth()+1) + '/' + date.getDate() + '/' + date.getYear().toString().slice(1)+' '+hours+':'+minutes+am;
}

function createBanner() {
    let date = '';
    date += '// --------------------------------------\n';
    date += '// \n';
    date += '//    _  _ _/ .  _  _/ /_ _  _  _        \n';
    date += '//   /_|/_ / /|//_  / / //_ /_// /_/     \n';
    date += '//   https://activetheory.net    _/      \n';
    date += '// \n';
    date += '// --------------------------------------\n';
    date += '//   ' + getDate() + '\n';
    date += '// --------------------------------------\n';
    date += '\n';
    return date;
}

function minifyJS(compiled) {
    let result = _terser.minify({ file: compiled }, _terserOpts);
    let minCode = result.code += '\nwindow._MINIFIED_=true;\nwindow._BUILT_=true;';
    let unminCode = new String(compiled);
    unminCode += '\nwindow._BUILT_=true;';
    return { minCode, unminCode };
}

function getArgs() {
    let arr = [];
    for (let i = 2; i < process.argv.length; i++) arr.push(process.argv[i]);
    return arr.join(' ');
}

function findConfig({ env, config }) {
    if (!config) {
        config = CONFIG_ORDER.filter((buildPath) => {
            return _fs.existsSync(_path.join(__dirname, buildPath));
        }).find(Boolean);
    }

    let configPath = _path.join(__dirname, config);
    if (!_fs.existsSync(configPath)) throw new Error(`config ${configPath} does not exist`);
    let fn = require(configPath);
    if (typeof fn === 'object') return fn;
    if (typeof fn === 'function') return fn({ env });
}
// END [UTILS]
