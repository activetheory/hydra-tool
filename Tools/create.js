const _HYDRA_ = __dirname.toLowerCase().split('/tools')[0];
const _HYDRA_DEVTOOLS_ = _HYDRA_ + '/devtools';
const _HYDRA_TOOLS_ = _HYDRA_ + '/tools/';

const _DIR_ = process.argv[2] + '/';
const _TOOLS_ = _DIR_ + 'Tools';
const _CREATE_ = _TOOLS_ + '/create/';
const _HTML_ = _DIR_ + 'HTML/';
const _JS_ = _HTML_ + 'assets/js/app/';

const _fs = require('fs');
const _execSync = require('child_process').execSync;
const _fsExtra = require('fs-extra');

(function() {
    copyTools();
    createDirs();
    copyFiles();
    if (process.argv[3]) runPreset();
    npmI();
})();

function copyTools() {

    // Don't replace Tools if exists
    if (!_fs.existsSync(_TOOLS_)) _fsExtra.copySync(_HYDRA_DEVTOOLS_, _TOOLS_);
}

function createDirs() {
    let dirs = [
        _HTML_,
        _HTML_ + 'assets',
        _HTML_ + 'assets/js',
        _HTML_ + 'assets/js/lib',
        _HTML_ + 'assets/js/hydra',
        _HTML_ + 'assets/css',
        _HTML_ + 'assets/fonts',
        _HTML_ + 'assets/images',
        _HTML_ + 'assets/shaders',
        _HTML_ + 'assets/runtime',
        _HTML_ + 'assets/data',
        _HTML_ + 'assets/geometry',
        _JS_,
        _JS_ + 'config',
        _JS_ + 'controllers',
        _JS_ + 'events',
        _JS_ + 'layouts',
        _JS_ + 'models',
        _JS_ + 'modules',
        _JS_ + 'util',
        _JS_ + 'views'
    ];

    dirs.forEach(dir => {
        if (!_fs.existsSync(dir)) {
            _fs.mkdirSync(dir);
            _fs.chmodSync(dir, '777');
        }
    });
}

function copyFiles() {
    let files = [
        [_CREATE_ + 'Main.js',              _JS_ + 'Main.js'],
        [_CREATE_ + 'style.css',            _HTML_ + 'assets/css/style.css'],
        [_CREATE_ + 'dev.js',               _HTML_ + 'assets/runtime/dev.js'],
        [_CREATE_ + 'es5-polyfill.js',      _HTML_ + 'assets/runtime/es5-polyfill.js'],
        [_CREATE_ + 'build.html',           _HTML_ + 'build.html'],
        [_CREATE_ + 'index.html',           _HTML_ + 'index.html'],
        [_CREATE_ + 'sw.js',                _HTML_ + 'sw.js'],
        [_CREATE_ + 'project.json',         _DIR_ + 'project.json'],
        [_CREATE_ + 'gitignore.txt',        _DIR_ + '.gitignore'],
        [_CREATE_ + 'package.json',         _DIR_ + 'package.json'],
    ];

    files.forEach(file => {
        if (!_fs.existsSync(file[1])) _fsExtra.copySync(file[0], file[1]);
    });

    // Create readme
    if (!_fs.existsSync(_DIR_ + '/readme')) _fs.writeFileSync(_DIR_ + '/README.md', `Readme \nfolder: ${process.argv[2].split('/').splice(-1)[0]}`);

    _fsExtra.removeSync(_CREATE_);
}

function runPreset() {
    _execSync(`node ${_HYDRA_TOOLS_}preset ${_DIR_} ${process.argv[3]}`, {stdio: 'inherit'});
}

function npmI() {
    _execSync(`npm i`, {stdio: 'inherit', cwd: _TOOLS_});
}
