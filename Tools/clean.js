const _DIR_ = __dirname.split('/Tools')[0];
const _HTML_ = _DIR_ + '/HTML/';
const _TOOLS_ = _DIR_ + '/Tools/';
const _JS_ = _DIR_ + '/HTML/assets/js/';
const _BUILD_ = _DIR_ + '/Build/';

_fs = require('fs-extra');

_fs.removeSync(`${_JS_}app/modules`);
_fs.removeSync(`${_JS_}app/config/Assets.js`);
_fs.removeSync(`${_JS_}app/hydra`);

_fs.mkdirsSync(`${_JS_}app/modules`);
_fs.mkdirsSync(`${_JS_}app/hydra`);
