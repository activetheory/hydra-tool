const _DIR_ = __dirname.toLowerCase().split('/tools')[0];
const _HTML_ = _DIR_ + '/html/';
const _TOOLS_ = _DIR_ + '/tools/';
const _APP_ = require('./findAppDir')();
const _SHADERS_ = _HTML_ + 'assets/shaders/';

const _fs = require('fs');
const _execSync = require('child_process').execSync;

const _config = require(_DIR_ + '/project.json');

// Pass second argument to force update (`$ node hydra y`)
const _isFullUpdate = process.argv[2];
const _missingModules = [];

(function() {
    checkModules('modules');
    checkModules('gl');

    // Exit if nothing to update
    if (!_isFullUpdate && !_missingModules.length) return;
    updateModules();
})();

function checkModules(type) {
    let modules = _config[type];

    modules.forEach(mod => {
        let localPath = `${type == 'gl' ? _SHADERS_ : _APP_}modules/${mod}`;
        if (!_isFullUpdate && _fs.existsSync(localPath)) return;
        _missingModules.push({type, mod});
    });
}

function updateModules() {
    _missingModules.forEach(mod => {
        let script = _TOOLS_ + (mod.type == 'gl' ? 'gl' : 'import');
        _execSync(`node ${script} ${mod.mod}`, {stdio: 'inherit'});
    });
}